FROM python:3.7.7-alpine3.10
RUN apk update && apk add py-pip && pip install redis
WORKDIR /home/git/Luminarias
COPY ./script.py .
CMD [ "python", "./script.py" ]
#docker build -t "miimagen" .